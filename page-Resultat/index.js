let nom = document.getElementById("nom");
let prenom = document.getElementById("prenom");
let formation = document.getElementById("listeDeroulante");
let dateAbsence = document.getElementById("dateAbsence");
let heureA = document.getElementById("heureA");
let dateAbsenceDu = document.getElementById("dateAbsenceDu");
let dateAbsenceAu = document.getElementById("dateAbsenceAu");
let NumAbsence = document.getElementById("NumAbsence");
let radioButton = document.getElementById("radioButton");
let nomDeux = document.getElementById("nomDeux");

function recupererDonnee(cle) {
  let donnee = window.location.search;
  donnee = donnee.substring(1);
  donnee = donnee.split("&");

  let value;
  donnee.forEach((element) => {
    element = element.split("=");
    // console.log(element);
    if (element[0] === cle) value = element[1];
  });
  return value;
}

if (
  recupererDonnee("dateAbsence") == null ||
  recupererDonnee("dateAbsence") == ""
) {
  const div_heure = document.querySelector(".heure");
  div_heure.classList.add("active");
} else {
  const div_jour = document.querySelector(".jour");
  div_jour.classList.add("active");
}
nom.value = recupererDonnee("nom");
prenom.value = recupererDonnee("prenom");
formation.value = recupererDonnee("formation");
dateAbsence.value = recupererDonnee("dateAbsence");
heureDe.value = recupererDonnee("heureDe");
heureA.value = recupererDonnee("heureA");
dateAbsenceDu.value = recupererDonnee("dateAbsenceDu");
dateAbsenceAu.value = recupererDonnee("dateAbsenceAu");
NumAbsence.value = recupererDonnee("NumAbsence");

nomDeux.value = recupererDonnee("nom") + " " + recupererDonnee("prenom");

const form = document.querySelector("#form");
const radi = document.getElementsByName("myRadio");

function getRadioCheckedValue(radi) {
  for (i = 0; i < radi.length; i++) {
    if (radi[i].checked) {
      return radi[i].value;
    }
  }
}

// *****************************************Button Submit******************************************

// form.addEventListener("submit", (e) => {

// });

/******************************************************** Animation des bubbles ***********************************************************/

// une fonction pour créer des bulles
const bubbleMaker = () => {
  // creating a span from JavaScript
  const bubble = document.createElement("span");
  // Insert bubble to the classList and then to the body
  bubble.classList.add("bubble");
  document.body.appendChild(bubble);

  // randomiser la taille des bulles
  const size = Math.random() * 200 + 100 + "px";
  bubble.style.height = size;
  bubble.style.width = size;

  // randomiser le positionnement des bulles
  bubble.style.top = Math.random() * 100 + 50 + "%";
  bubble.style.left = Math.random() * 100 + "%";

  // plusMinus is for that the bubble's moving should be in two direction, left or right
  const plusMinus = Math.random() > 0.5 ? 1 : -1;
  bubble.style.setProperty("--left", Math.random() * 100 * plusMinus + "%");

  setTimeout(() => {
    bubble.remove();
  }, 8000);
};

// pour créer plusieurs bulles
setInterval(bubbleMaker, 1000);

// *****************************************Connection en fichier json**********************************************

fetch("../motif.json")
  .then(function (res) {
    if (res.ok) {
      return res.json();
    }
  })
  .then(function (value) {
    const radiooo = recupererDonnee("myRadio");
    console.log(radiooo);
    radioButton.value = chercher(value, radiooo);
    console.log(radioButton.value);
  })
  .catch(function (err) {
    console.error(err);
  });

function chercher(json, pId) {
  let variable = json["members"];
  let value;
  console.log(variable);
  variable.forEach((elem) => {
    elem["options"].forEach((opt) => {
      if (opt["op_id"] === pId) value = opt["op_label"];
    });
  });
  return value;
}
