const form = document.querySelector("#form");
const radi = document.getElementsByName("myRadio");

function getRadioCheckedValue(radi) {
  for (i = 0; i < radi.length; i++) {
    if (radi[i].checked) {
      return radi[i].value;
    }
  }
}

// **********************************Function absence***************************************
function absence() {
  const dateAbsence = document.querySelector("#dateAbsence");

  if (dateAbsence.value == null || dateAbsence.value == "") {
    const jourAbsenceDepart = document.querySelector("#dateAbsenceDu");
    if (jourAbsenceDepart.value == null || jourAbsenceDepart.value == "") {
      alert("Veuillez choisir la date de l'absence");
    } else {
      const jourAbsenceFini = document.querySelector("#dateAbsenceAu");
      if (jourAbsenceFini.value == null || jourAbsenceFini.value == "") {
        alert("Veuillez vérifier la date de l'absence");
      } else {
        const joursAbsence = document.querySelector("#NumAbsence");
        if (
          joursAbsence.value == null ||
          joursAbsence.value == "" ||
          joursAbsence.value < 1
        ) {
          alert("Veuillez vérifier la date de l'absence");
        } else {
          return (
            "Date début d'absence: " +
            jourAbsenceDepart.value +
            " Date de retour: " +
            jourAbsenceFini.value +
            " Nombre de jour: " +
            joursAbsence.value
          );
        }
      }
    }
  } else {
    const heureDepart = document.querySelector("#heureDe");
    if (
      heureDepart.value >= 17 ||
      heureDepart.value < 8 ||
      heureDepart.value == null ||
      heureDepart.value == ""
    ) {
      return alert("Veuillez vérifier l'heure de départ!!!");
    } else {
      const heureFini = document.querySelector("#heureA");
      if (
        // heureDepart.value >= heureFini.value ||
        heureFini.value < 9 ||
        heureFini.value > 17 ||
        heureFini.value == null ||
        heureFini.value == ""
      ) {
        return alert("Veuillez vérifier l'heure de fin absence!!!");
      } else {
        return (
          "Date d'absence: " +
          dateAbsence.value +
          " Heure de départ: " +
          heureDepart.value +
          " Heure de fin: " +
          heureFini.value
        );
      }
    }
  }
}

// ************************************ Générer le fichier Json ****************************************

// function genererJson() {
fetch("./motif.json")
  .then(function (res) {
    if (res.ok) {
      return res.json();
    }
  })
  .then(function (value) {
    console.log(value);
    displayData(value);
  })
  .catch(function (err) {
    console.error(err);
  });
// }

function displayData(data) {
  const outputDiv = document.getElementById("output");
  outputDiv.innerHTML = `<h1 class="topTitle">${data.topTitle}</h1>`;
  outputDiv.innerHTML += "<ul>";
  data.members.forEach((member) => {
    outputDiv.innerHTML += `<li class="titres-motif" id="${member.id}" name="${member.name}">${member.name} : ${member.title}</li>`;
    member.options.forEach((opt) => {
      outputDiv.innerHTML += `<li class="radio-button"><input name="myRadio" type="radio" value=${opt.op_id} id="${opt.op_id}"/>${opt.op_label}</li>`;
    });
  });
  outputDiv.innerHTML += "</ul>";
  outputDiv.classList.add("class-motif");
  //   outputDiv.id = "super";
}

// *******************************************************Button Motif Absence ************************************************************

const btn_json = document.querySelector(".btn-json");
btn_json.addEventListener("click", (e) => {
  e.preventDefault();
  const outputMotif = document.querySelector(".hide");
  outputMotif.classList.toggle("active");
  // genererJson();
});

/******************************************************** Animation des bubbles ***********************************************************/

// une fonction pour créer des bulles
const bubbleMaker = () => {
  // creating a span from JavaScript
  const bubble = document.createElement("span");
  // Insert bubble to the classList and then to the body
  bubble.classList.add("bubble");
  document.body.appendChild(bubble);

  // randomiser la taille des bulles
  const size = Math.random() * 200 + 100 + "px";
  bubble.style.height = size;
  bubble.style.width = size;

  // randomiser le positionnement des bulles
  bubble.style.top = Math.random() * 100 + 50 + "%";
  bubble.style.left = Math.random() * 100 + "%";

  // plusMinus is for that the bubble's moving should be in two direction, left or right
  const plusMinus = Math.random() > 0.5 ? 1 : -1;
  bubble.style.setProperty("--left", Math.random() * 100 * plusMinus + "%");

  setTimeout(() => {
    bubble.remove();
  }, 8000);
};

// pour créer plusieurs bulles
setInterval(bubbleMaker, 1000);
